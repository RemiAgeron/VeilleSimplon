=== Plugin Name ===
Contributors: dnorman
Donate link: 
Tags: avatar, post
Requires at least: 2.9
Tested up to: 5.2.4
Stable tag: 0.1

Displays the avatar for a post's author, without having to hack it into the theme.

== Description ==

A simple plugin to display the avatar of a post author, without having to modify the theme to do so. Useful for a multisite installation, where users can’t edit themes and you want to be able to update themes without having to re-hack support for post avatars into place. Handy for educational class-blog sites, etc...

== Installation ==

1. Upload `wp-display-avatars.php` to the `/wp-content/plugins/` directory (or, just uplaod the whole `display-avatars` directory)
1. Activate the plugin through the 'Plugins' menu in WordPress
1. There is no step 3, yet...

== Frequently Asked Questions ==

= What does it do? =

All it does is cause the author's avatar to be displayed in the title of blog posts. It'll use whatever avatar is supported by WordPress, and plays nicely with the Local Avatar plugin.

== Screenshots ==

1. Look! The Author's avatar! Beside the title for a blog post!

== Changelog ==

= 0.1 =
* first version. no settings (yet) but that will change.

== Upgrade Notice ==

Nothing. The Readme validator just complains if this section isn't there...