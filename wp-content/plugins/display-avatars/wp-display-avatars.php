<?php
/*
Plugin Name: Display Avatars
Plugin URI: 
Author: D'Arcy Norman
Author URI: http://www.darcynorman.net/
Description: Display the avatar for blog post authors and commenters, using local WordPress avatar system via the Avatars Plugin http://www.sterling-adventures.co.uk/blog/2008/03/01/avatars-plugin/
Version: 0.1
Text Domain: displayavatars
*/

	add_filter('the_title', 'displayPostAuthor', 10, 1);
	add_filter('get_comment_author', 'displayCommentAuthor', 10, 1);
	add_filter('next_post_link', 'removeExtraAvatars', 10, 1);
	add_filter('previous_post_link', 'removeExtraAvatars', 10, 1);
	
	function displayPostAuthor($text = null) {
		if (!in_the_loop()) return $text;
		if (is_page()) return $text;
		
		$avatarcontent = '';
		if (function_exists( 'get_avatar' ) ) {
			$avatarcontent .= '<div class="post-avatar" style="float: right;" >';
			$avatarcontent .= get_avatar( get_the_author_meta('user_email') ,64,null, $text . ' avatar');
			$avatarcontent .= '</div>';

		} else {
			$avatarcontent .= 'no get_avatar';
		}

		return $text . $avatarcontent;
	}

	function displayCommentAuthor($text = null) {
		$avatarcontent = '';
	
		return $text; // . $avatarcontent;
	}

	function removeExtraAvatars($text = null) {
		return preg_replace('/<div(.*)<\/div>/U', '', $text);
		//return '['.$text.']';
	}